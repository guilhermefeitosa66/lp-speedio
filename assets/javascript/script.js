// TRANSLATE LABEL IN PLAN SELECTION TO CENTER OF THE PARENT
// REASON EXPLAINED IN assets/sass/_section_plan_selection.scss
let translateLabelToCenter = function () {
    const parents = document.querySelectorAll('body > section.plan-selection > div.plans > div > div')

    parents.forEach(parent => {
        // find a label into .plan
        let child = parent.getElementsByTagName('label')

        if (child.length > 0) {
            let translateChild = (parent.clientWidth / 2) - (child[0].clientWidth / 2)
            // console.dir(child[0])
            child[0].style.transform = `translate(${translateChild}px, -50%)`
        }
    })
}

let sliderThumbControl = function () {
    // js for moving slide range thumb
    let range = document.querySelector('input[type="range"]')
    let rangeLabel = document.querySelector('.range-slider label')

    range.addEventListener("change", function () {
        let total = this.max
        let a = (this.value * 100) / total
        // let b = ((total - this.value) * 100) / total
        let sizeBar = this.offsetWidth
        let positionRangeSliderThumb = 0
        // TODO: Refact this peace of code        
        if (this.value < 1) {
            positionRangeSliderThumb = "10rem"
        } else if (this.value < this.max) {
            positionRangeSliderThumb = (((a * sizeBar) / 100) + 80) + "px"
        } else {
            positionRangeSliderThumb = ((a * sizeBar) / 100) + "px"
        }

        // dont move label in mobile screens
        if (document.defaultView.screen.availWidth > 480) {
            rangeLabel.style.left = positionRangeSliderThumb
        }

        this.style.backgroundImage = `linear-gradient(to right, #F2994A ${a}%, #F2F2F2 ${a}.5%, #F2F2F2 100%)`
    });
}

let planSelectionControl = function () {
    // js for selecting plans
    document.querySelectorAll('.list-plans .plan').forEach(item => {
        item.addEventListener('click', e => {
            document.querySelectorAll('.list-plans .plan').forEach(plan => {
                plan.classList.remove('selected')
            })
            item.classList.add('selected')
        })
    })
}

// toggle banner visibility after scroll the page   
let toggleUnpinAtScroll = function () {
    let banner = document.querySelector("section.banner")
    let y = window.scrollY
    let fixeAtHeight = banner.getBoundingClientRect().top

    if (y < fixeAtHeight) {
        banner.classList.remove('unpin-banner')
    } else {
        banner.classList.add('unpin-banner')
    }
}

// accordion control
let accordionControl = function () {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {

            this.classList.toggle("active");

            var panel = this.nextElementSibling;

            panel.classList.toggle("active-panel");

            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
}

let init = function () {
    // center label at the first time
    translateLabelToCenter()

    // init slider thumb control
    sliderThumbControl()

    // init plan selection control
    planSelectionControl()

    // init accordion control
    accordionControl()

    // run toggleUnpinAtScroll() function when the page was scrolled
    window.addEventListener("scroll", toggleUnpinAtScroll)

    // run translateLabelToCenter() function when the page was resized
    window.addEventListener('resize', translateLabelToCenter)
}

// run this function when the page was loaded
window.onload = function () {
    console.log("Page loaded!")

    // init functions when page was complete loaded
    init()
}