# Avaliação de Nível Técnico de Front-End - Speedio

    > This README is in Brazilian Portuguese Language 🇧🇷

O objetivo deste README é explicar para o avaliador como foi realizada a implementação desta avaliação, de maneira que o responsável pela revisão possa compreender também o ponto de vista do avaliado em questão.

Compreendo que é bastante comum que equipes de desenvolvimento de software sejam composta por pessoas de nacionalidades diversas e que a língua inglesa, em regra, é adotada por padrão. Mas estou supondo que meu avaliador seja um brasileiro e afim de facilitar a leitura deste texto, optei por escrevê-lo em Português.

# Tecnologias utilizadas

Tendo em vista que o objetivo é avaliar o domínio das habilidades de front-end, optei por não adotar nenhuma biblioteca ou framework CSS, com exceção do **normalize.css**. Então todo o código fora criado a partir do zero, sem a utilização de bibliotecas, framework ou código de terceiros.

Para otimizar o processo de escrita do CSS e organização dos componentes em arquivos separados, foi adota o **pré-processador de CSS Sass**.

O editor de códigos utilizado para esse trabalho foi o **Visual Studio Code** da Microsoft com o auxílio da extensão **Live Server** (para prover um servidor web de desenvolvimento com auto-reload automático a medida em são detectadas novas alterações).

# Sugestão para visualizar a página construída

1 - Clone este repositório para seu computador:

```bash
git clone https://gitlab.com/guilhermefeitosa66/lp-speedio.git
```

2 - Adicione a pasta **lp-speedio** ao vscode
3 - Instale a extensão **Live Server** em seu editor e clique no ícone inferior **Go Live**
4 - Acesse o servidor local em: **http://localhost:5500/index.html** em um browser _(preferencialmente o google chrome, para utilização das ferramentas de desenvolvimento e debug, fica a seu critério)_

Durante o desenvolvimento da página utilizei dois modos de visualização, para a implementação dos dois layouts proposto, um desktop e outro mobile, para comparação das medidas de forma precisa, visualização desktop com resolução **1280x720** e visualização mobile com resolução **360x640**. Como o layout é responsivo e fiz a conversão de todas as unidades de medidas absolutas para medidas relativas. Desta forma será possível comparar as medidas do layout proposto com exatidão, já que se a resolução for em valores intermediários, o layout se manterá com as mesmas proporções, porém com medidas absolutas diferentes.

Com o intuito de facilitar o calculo dos valores em na medida relativa **rem** que usa como base o tamanho do texto definido na raiz da página (html). defini como valor fixo global 10px

```css
/* arquivo: assets/sass/_base.scss */
$rem-base-text-size: 10px

html {
	...
    font-size: $rem-base-text-size;
    ...
}
```

# TODO

O que ainda falta ser corrigido/implementado:

- A seleção dos planos ao mover o _slider_
- O script que fixa o banner no rodapé da página quando estiver com o layout na versão mobile
- Refatorar novamente o código para enxugar alguns trechos de código repetidos ou sem uso

## Estrutura de arquivos

```console
├── README.md
├── assets
│ ├── icons
│ │ ├── arrow-right.svg
│ │ ├── arrow-up.svg
│ │ ├── check-circle.svg
│ │ ├── check-orange.svg
│ │ ├── check.svg
│ │ ├── heart.svg
│ │ ├── logo-full.svg
│ │ ├── logo-short.svg
│ │ ├── play.svg
│ │ ├── range-pin.svg
│ │ ├── social-icon-facebook.svg
│ │ ├── social-icon-instagram.svg
│ │ ├── social-icon-linkedin.svg
│ │ ├── social-icon-whatsapp.svg
│ │ └── social-icon-youtube.svg
│ ├── images
│ │ └── video-cover.png
│ ├── javascript
│ │ └── script.js
│ ├── sass
│ │ ├── _accordion.scss
│ │ ├── _base.scss
│ │ ├── _main_navbar.scss
│ │ ├── _section_banner.scss
│ │ ├── _section_faq.scss
│ │ ├── _section_footer.scss
│ │ ├── _section_footnote.scss
│ │ ├── _section_main.scss
│ │ ├── _section_plan_selection.scss
│ │ ├── _section_video.scss
│ │ └── style.scss
│ └── stylesheets
│ ├── normalize.css
│ ├── style.css
│ └── style.css.map
└── index.html
```

Foi um imenso prazer participar desse desafio, deixo aqui um forte abraço a todos os envolvidos na avaliação deste trabalho ❤️
